// Object
//  - a data type that is used to represent real world objects.

// Creating onjects using object initializer/literal rotation
/*
	- Syntax:
		let objectName = {
			KeyA: valueA, 
			keyB: valueB
		}
*/

let cellphone = {
	name: 'Nokia 3315',
	manufactureDate: 1999
}

console.log("Result from creating objects using initializers/literal notation");
console.log(cellphone);
console.log(typeof cellphone);

// Creating objects using a construstor function

function Laptop(name, manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate;
}

// Creating a unique instance of the laptop object
// Using "new"  operator creates an intances of an object

let laptop = new Laptop('Lenovo', 2008);
console.log('Result from creating objects using objects constructor');
console.log(laptop);

let myLaptop = new Laptop('Mac Air', 2020);
console.log('Result from creating objects using objects constructor');
console.log(myLaptop);

let oldLaptop = Laptop('Alienware', 2016);
console.log('Result from creating objects without the new keyword:');
console.log(oldLaptop);

// Create empty objects
let computer = {};
let myComputer = new Object();

// Accessing Object Prperties
// Using dot notation
console.log('Result from dot notation: ' + myLaptop.name);

console.log('Result from dot notation: ' + laptop.manufactureDate);

// Using square bracket notation
console.log('Result from square bracket notation:' + laptop['name']);

// Accessing array object

let array = [laptop, myLaptop];

console.log(array[0]['name']);
console.log(array[1].name);

// Initializing/adding/deleting/reassigning object prepertie

let car = {};

// Initialize/add object properties using dot notation
car.name = 'Honda Civic';
console.log('Result from adding properties using dot notation');
console.log(car);

car ['manufacture date'] = 2019;
console.log(car['manufacture date']);
console.log(car['Manufacture Date']);

console.log(car.manufactureDate);
console.log('Result from adding properties uaing square bracket notation');
console.log(car);

// Deleting object properties
delete car['manufacture date'];
console.log('Result from deleting properties: ');
console.log(car);

//  Reassigning object properties
car.name = 'Dodge Charger R/T';
console.log('Result from reassigning properties: ');
console.log(car);

// Object methods
// - a method that is function which is a property of an object

let person = {
	name: 'John',
	talk: function(){
		console.log('Hello my name is ' + this.name);
	}
}
console.log(person);
console.log('Result from object method: ');
person.talk();

// Adding methids to objects
person.walk = function(){
	console.log(this.name + ' walked 25 steps forward');
}
person.walk();

person.sing = function(){
	console.log(this.name + ' has sang 30 song');
}
person.sing();

let friend = {
	firstName: 'Joe',
	lastName: 'Smith',
	address: {
		city: 'Austin',
		country: 'US'
	},
	emails: ['joe@mail.com', 'joesmtih@email.net'],
	introduce: function(){
		console.log('Hello my name is ' + this.firstName + ' ' + this.lastName);
	}
}
friend.introduce();

// Real world application of objects

// Using object literal to create multiple kinds of pokemon

let myPokemon = {
	name: 'Pikachu',
	level: 3,
	health: 100,
	attack: 50, 
	tackle: function(){
		console.log("This Pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduce to _targetPokemonHealth_");
	},
	faint: function(){
		console.log("Pokemon fainted");
	}
}

// Creating an object constructor to help in creating an in a faster process
function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	// Methods
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		console.log("targetPokemon's health is now reduced to _targetPokemonHealth_");
	}
	this.faint = function (){
		console.log(this.name + ' fainted');
	}
}

let pikachu = new Pokemon('Pikachu', 16);
let ratata = new Pokemon('Ratata', 8);

pikachu.tackle(ratata);